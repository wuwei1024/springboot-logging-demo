package com.demo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author wuwei
 * @date 2019/1/25 10:30
 * SpringBoot应用启动事件，执行先后顺序如下：
 * ApplicationStartingEvent
 * ApplicationEnvironmentPreparedEvent
 * ApplicationPreparedEvent
 * ApplicationStartedEvent <= SpringBoot2.0新增的事件
 * ApplicationRunner
 * CommandLineRunner
 * ApplicationReadyEvent
 * ApplicationFailedEvent
 */
@Slf4j
@Component
public class MyApplicationStartupEventListener {

    @Autowired
    private Environment environment;

    @Async
    @EventListener
    public void applicationStartedEvent(ApplicationStartedEvent event) {
        log.info("【Application启动事件4】ApplicationStartedEvent");
        String appName = environment.getProperty("spring.application.name");
        log.info("Application name is {}", appName);
        log.info("Current thread name is {}", Thread.currentThread().getName());
    }

    @Async
    @EventListener
    public void applicationReadyEvent(ApplicationReadyEvent event) {
        log.info("【Application启动事件5】ApplicationReadyEvent");
        String port = environment.getProperty("server.port");
        String str = String.format("Application started on port: %s and at time: %d", port, event.getTimestamp());
        log.trace("[Logging Level: trace] {}", str);
        log.debug("[Logging Level: debug] {}", str);
        log.info("[Logging Level: info] {}", str);
        log.warn("[Logging Level: warn] {}", str);
        log.error("[Logging Level: error] {}", str);
        log.info("Current thread name is {}", Thread.currentThread().getName());
    }

    @Async
    @EventListener
    public void applicationFailedEvent(ApplicationFailedEvent event) {
        log.error("【Application启动事件6】ApplicationFailedEvent");
    }
}
