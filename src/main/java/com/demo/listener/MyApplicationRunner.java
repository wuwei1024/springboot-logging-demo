package com.demo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wuwei
 * @date 2019/1/25 17:42
 * 默认情况下ApplicationRunner比CommandLineRunner先执行
 * 通过@Order注解可以调整执行的先后顺序，值越小，越先执行
 */
@Slf4j
@Order(1)
@Component
public class MyApplicationRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("======MyApplicationRunner======");
    }
}
