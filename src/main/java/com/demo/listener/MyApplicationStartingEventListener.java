package com.demo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;

/**
 * @author wuwei
 * @date 2019/1/25 17:01
 * 项目启动事件监听器
 */
@Slf4j
public class MyApplicationStartingEventListener implements ApplicationListener<ApplicationStartingEvent> {

    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        SpringApplication application = event.getSpringApplication();
        application.setBannerMode(Banner.Mode.OFF);
        //log.info("【Application启动事件1】ApplicationStartingEvent");
        System.out.println("【Application启动事件1】ApplicationStartingEvent");
    }
}