package com.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author wuwei
 * @date 2019/1/24 17:52
 */
@Slf4j
@Component
public class AsyncScheduleTask {

    @Async
    @Scheduled(fixedRate = 5 * 1000)
    public void task1() {
        log.info("异步定时任务1，线程名：{}", Thread.currentThread().getName());
    }

    @Async
    @Scheduled(fixedRate = 10 * 1000)
    public void task2() {
        log.info("异步定时任务2，线程名：{}", Thread.currentThread().getName());
    }
}
