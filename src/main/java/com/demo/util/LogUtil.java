package com.demo.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author wuwei
 * @date 2019/1/25 14:48
 * 修改日志的级别工具类
 * 参考：https://blog.csdn.net/cowbin2012/article/details/85235931
 */
public class LogUtil {

    /**
     * 设置所有的日志级别
     *
     * @param level 日志级别
     */
    public static void setAllLogLevel(String level) {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        List<Logger> loggerList = loggerContext.getLoggerList();
        for (Logger logger : loggerList) {
            logger.setLevel(Level.toLevel(level));
        }
    }

    /**
     * 修改某一个包下的日志级别
     *
     * @param name  需要修改日志级别的包名或类名
     * @param level 新的日志级别
     */
    public static void setLogLevel(String name, String level) {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger logger = loggerContext.getLogger(name);
        if (logger != null) logger.setLevel(Level.toLevel(level));
    }
}
