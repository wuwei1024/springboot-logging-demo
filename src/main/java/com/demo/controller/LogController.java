package com.demo.controller;

import com.demo.util.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuwei
 * @date 2019/1/25 11:30
 * 在不重启应用的情况下，动态修改SpringBoot应用日志级别的两种方法：
 * 1、通过SpringBoot提供的actuator来修改
 * 查询controller包的日志级别：http://localhost:${port}/api/actuator/loggers/com.demo.controller
 * 返回结果如下：
 * {
 * "configuredLevel": "DEBUG",
 * "effectiveLevel": "DEBUG"
 * }
 * 访问接口：http://localhost:${port}/api/log/testLogLevel，控制台会输出DEBUG级别以上所有日志
 * <p>
 * 修改controller包的日志级别：http://localhost:${port}/api/actuator/loggers/com.demo.controller
 * POST请求的参数为JSON格式，结构如下：
 * {
 * "configuredLevel": "INFO"
 * }
 * <p>
 * 再次查询controller包的日志级别：http://localhost:${port}/api/actuator/loggers/com.demo.controller
 * 返回结果如下：
 * {
 * "configuredLevel": "INFO",
 * "effectiveLevel": "INFO"
 * }
 * <p>
 * 再次访问接口：http://localhost:${port}/api/log/testLogLevel，控制台只输出INFO级别以上的日志
 */
@Slf4j
@RestController
@RequestMapping("/log")
public class LogController {

    @PostMapping("/testLogLevel")
    public String testLogLevel() {
        log.debug("Logger Level ：DEBUG");
        log.info("Logger Level ：INFO");
        log.error("Logger Level ：ERROR");
        return "logging success";
    }

    /**
     * 2、通过自己定义的REST API来修改
     *
     * @param level 新设置的日志级别
     * @param name  需要修改日志级别的包名或类名
     * @return
     */
    @PostMapping("/changeLogLevel")
    public String changeLogLevel(@RequestParam("level") String level,
                                 @RequestParam(value = "name", required = false) String name) {
        if (StringUtils.isEmpty(name)) return "level cannot be empty";
        if (StringUtils.isEmpty(name)) {
            LogUtil.setAllLogLevel(level);
        } else {
            LogUtil.setLogLevel(name, level);
        }
        return "logging set success";
    }
}